# Maintainer: Caleb Connolly <caleb@connolly.tech>
# Co-Maintainer: Joel Selvaraj <jo@jsfamily.in>
# Stable Linux kernel with patches for SDM845 devices
# Kernel config based on: arch/arm64/configs/defconfig and sdm845.config

_flavor="postmarketos-qcom-sdm845"
pkgname=linux-$_flavor
pkgver=5.10.12
pkgrel=1
pkgdesc="Mainline Kernel fork for SDM845 devices"
arch="aarch64"
_carch="arm64"
url="https://gitlab.com/sdm845-mainline/sdm845-linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native"
makedepends="bison findutils flex installkernel openssl-dev perl"

_repo="sdm845-linux"
_config="config-$_flavor.$arch"
_commit="77791dbc4be08d072b1af3378c4490723d40f9e6"

# Source
source="
	$_repo-$_commit.tar.gz::https://gitlab.com/sdm845-mainline/$_repo/-/archive/$_commit/$_repo-$_commit.tar.gz
	$_config
"
builddir="$srcdir/$_repo-$_commit"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$arch" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot/ \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="77d01126afbb949d98e6c5397ae4170a219b2a81af17d83e8f67cc619722a021da2ab4363c13100f19650e00e4fa869b7130770cd0f328936141f593fb562797  sdm845-linux-77791dbc4be08d072b1af3378c4490723d40f9e6.tar.gz
1b96db9ceb6f5832d9d8c7ac4595a983e2a0f47b49da1ccb09db2d4e2d24974074883c30298b68a002c67dda64f1efe39932c48fdfd19fe87142d2f568a17317  config-postmarketos-qcom-sdm845.aarch64"
